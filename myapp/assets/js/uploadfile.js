function upload_file(){
  $('#uploadFileForm').ajaxForm({
    beforeSubmit: function() {
      $('#progressFile').show();
      $('#progressFile.progress-bar').width('0%');
    },
    uploadProgress: function(event, position, total, percentComplete) {
      $('#progressFile.progress-bar').width(percentComplete + '%');
    },
    success: function() {
      $('#progressFile').hide();
    }
  });
}