<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('user_model');
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
$this->output->set_header('Pragma: no-cache');
$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        if($this->session->userdata('logged_in') !== TRUE)
            redirect(base_url('auth/login'));
    }

    public function index(){
        $this->load->model('upload_model');
        $user = $this->user_model->get_user($this->session->userdata('email'));
        
        $filter = $this->input->get('filter');
        if($filter == '0' || $filter == '1' || $filter == '2')
            $items = $this->upload_model->get_filter_status_by_userid($filter, $user['userid']);
        else
            $items = $this->upload_model->get_all_by_userid($user['userid']);

        $document_count = $this->upload_model->get_document_count_by_userid($user['userid']);
        
        $data = array(
            'profile_completed' => $this->user_model->get_completed_profile($user),
            'file_uploaded' => $document_count['all'],
            'verified' => $document_count['verified'],
            'pending' => $document_count['pending'],
            'items' => $items,
        );
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit_profile(){
        $this->load->library('form_validation');
        
        // CSRF for secure form
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        $data['csrf'] = $csrf;

        // Pre-fill all form
        $user = $this->user_model->get_user($this->session->userdata('email'));
        $data['email'] = $user['email'];
        $data['username'] = $user['username'];
        $data['fname'] = $user['first_name'];
        $data['lname'] = $user['last_name'];
        $data['gender'] = $user['gender'];
        $data['phone'] = $user['phone_no'];
        $data['birthday'] = $user['birthday'];
        $data['birthplace'] = $user['birth_place'];
        $data['address'] = $user['address'];
        $data['bio'] = $user['bio'];

        if($this->form_validation->run('edit_profile') === FALSE){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('user/edit_profile', $data);
        } else {
            if($this->user_model->update_profile($this->session->userdata('email'))){
                $this->session->set_flashdata('feedback', 'Profile successfully updated');
                $this->session->set_flashdata('feedback_class', 'alert-success');
            } else {
                $this->session->set_flashdata('feedback', 'Sorry! Please try again.');
                $this->session->set_flashdata('feedback_class', 'alert-danger');
            }
            redirect('user/edit_profile');
        }
    }

    public function edit_photo(){
        $this->load->library('form_validation');
        
        // CSRF for secure form
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        $data['csrf'] = $csrf;

        // Pre-fill all form
        $user = $this->user_model->get_user($this->session->userdata('email'));
        $data['photo'] = $user['photo'];

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('user/edit_photo', $data);
        $this->load->view('templates/footer');
    }

    public function do_upload_photo(){
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('photo')){
            $this->session->set_flashdata('feedback', 'Sorry! Please try again.');
            $this->session->set_flashdata('feedback_class', 'alert-danger');
        } else {
            $this->user_model->update_photo(
                $this->session->userdata('email'),
                $this->upload->data('file_name')
            );

            $this->session->set_flashdata('feedback', 'Profile successfully updated');
            $this->session->set_flashdata('feedback_class', 'alert-success');
        }

        redirect(base_url('user/edit_profile/photo'));
    }
}