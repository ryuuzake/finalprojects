<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('upload_model');
        $this->load->library('pagination');
        $this->output->delete_cache();
    }

    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        if($this->input->get('q')){
            $config['base_url'] = base_url('search/page/');
            $config['total_rows'] = $this->upload_model->search_simple();
            $config['per_page'] = 10;

            $this->pagination->initialize($config);

            $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data['search'] = $this->input->get('q');
            $data['items'] = $this->upload_model->search_simple($config['per_page'], $data['page']);
            $data['pagination'] = $this->pagination->create_links();

            $this->load->view('search/result', $data);
        } else {
            $this->load->view('search/simple');
        }
        $this->load->view('templates/footer');
    }

    public function advanced(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        if($this->input->get()) {
            $data['search'] = $this->input->get('q');
            $data['items'] = array();
            $this->load->view('search/result', $data);
        } else {
            $this->load->view('search/advanced');
        }
        $this->load->view('templates/footer');
    }

    public function by_year($slug = NULL){
        $data['items'] = array();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('search/by-year', $data);
        $this->load->view('templates/footer');
    }

    public function by_category($slug = NULL){
        $data['items'] = array();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('search/by-category', $data);
        $this->load->view('templates/footer');
    }
}