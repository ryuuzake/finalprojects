<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Repo extends CI_Controller {

    public function index(){
        $this->load->model('document_model');

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('repo/home');
        $this->load->view('templates/footer');
    }

    public function about(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('repo/about');
        $this->load->view('templates/footer');
    }
}