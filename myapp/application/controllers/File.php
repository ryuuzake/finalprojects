<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('document_model');
        $this->load->model('upload_model');
        $this->load->model('category_model');
        $this->output->delete_cache();
    }

    public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('file/index');
        $this->load->view('templates/footer');
    }

    public function view($id = NULL) {
        $data['upload'] = $this->upload_model->get_by_id($id);
        $data['document'] = $this->document_model->get_by_id($data['upload']->doc_id);
        $data['category'] = $this->category_model->get_by_id($data['upload']->cat_id);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('file/view', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = NULL){
        if($this->session->userdata('logged_in') !== TRUE)
            redirect(base_url('auth/login'));

        $data['document'] = $this->document_model->get_by_id($id);
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('file/edit', $data);
        $this->load->view('templates/footer');
    }

    public function upload($id = NULL){
        if($this->session->userdata('logged_in') !== TRUE)
            redirect(base_url('auth/login'));

        $this->load->library('form_validation');

        // CSRF for secure form
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        $data['csrf'] = $csrf;
        
        if($id != NULL){
            // Upload Metadata
            if($this->form_validation->run('upload_metadata') === FALSE){
                $data['docid'] = $id;
                $data['categories'] = $this->category_model->get_all();
                $this->load->view('templates/header');
                $this->load->view('templates/navbar');
                $this->load->view('file/upload_metadata', $data);
                $this->load->view('templates/footer');
            } else {
                $uploadid = $this->upload_model->save();
                redirect(base_url("file/view/$uploadid"));
            }
        } else {
            // Upload File
            if($this->form_validation->run('upload_file') === FALSE){
                $this->load->view('templates/header');
                $this->load->view('templates/navbar');
                $this->load->view('file/upload', $data);
            } else {
                $doc_id = $this->document_model->save();
                redirect(base_url("file/upload/$doc_id/"));
            }
        }
    }
}