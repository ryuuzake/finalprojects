<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
    }

    public function login(){
        $this->load->library('form_validation');
        
        // CSRF for secure form
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        $data['csrf'] = $csrf;

        if($this->form_validation->run('login') === FALSE){
            $this->load->view('auth/login', $data);
        } else {
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $user = $this->user_model->get_user($email);

            if(password_verify($password, $user['password'])){
                $data_session = array(
                    'userid' => $user['userid'],
                    'name' => $user['username'],
                    'email' => $user['email'],
                    'full_name' => $user['first_name'].' '.$user['last_name'],
                    'photo' => base_url('uploads/'.$user['photo']),
                    'logged_in' => TRUE,
                    'role' => $this->user_model->get_role($user['userid'])['role']
                );
                $this->session->set_userdata($data_session);
                
                $this->session->set_flashdata('feedback', 'Registration Succesfull! Please Sign in to Continue');
                $this->session->set_flashdata('feedback_class', 'alert-success');
                
                redirect(base_url('repo'));
            } else {
                $this->session->set_flashdata('feedback', 'Sorry! Wrong Email or Password');
                $this->session->set_flashdata('feedback_class', 'alert-danger');

                $this->load->view('auth/login', $data);
            }
        }
    }

    public function register(){
        $this->load->library('form_validation');
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        $data['csrf'] = $csrf;

        if($this->form_validation->run('register') === FALSE){
            $this->load->view('auth/register', $data);
        } else {
            $email = $this->input->post('email');
            $password = password_hash($this->input->post('password'), PASSWORD_BCRYPT, ['cost' => 12]);

            if($this->user_model->create_user($email, $password)){
                $this->session->set_flashdata('feedback', 'Registration Succesfull! Please Sign in to Continue');
                $this->session->set_flashdata('feedback_class', 'alert-success');
            
                redirect(base_url('user'));
            } else {
                $this->session->set_flashdata('feedback', 'Sorry! Please try again.');
                $this->session->set_flashdata('feedback_class', 'alert-danger');

                $this->load->view('auth/register', $data);
            }
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url('auth/login'));
    }

    public function email_check($str){
        return ($this->user_model->get_user($str) == null);
    }
}