<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Simple Search</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                <li class="breadcrumb-item active">Simple Search</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Simple Search</h3>
          </div>
          <div class="card-body">
            <form>
              <div class="form-group row">
                <label for="q" class="col-sm-1 col-form-label pos_title_simple_search3">Item</label>
                <div class="col-sm-8 pos_box_simple_search1">
                  <input type="text" class="form-control" name="q" id="q" placeholder="Enter item you want to search">
                </div>
              </div>
              <div class="form-group row">
                <label for="order" class="col-sm-1 col-form-label pos_title_simple_search4">Order</label>
                <div class="col-sm-8 pos_box_simple_search2">
                  <select class="custom-select" name="order" id="order">
                    <option value="by-year-desc">by year (oldest first)</option>
                    <option value="by-year-asc" selected>by year (most recent first)</option>
                    <option value="by-author">by author</option>
                    <option value="by-title">by title</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm text-center pos_btn_simple_search1">
                  <button type="submit" class="btn btn-primary">Search</button>
                </div>
              </div>
            </form>
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
            <p class="text-center"><a href="<?= base_url('search/advanced') ?>">Click here to go to Advanced Search</a></p>
          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->