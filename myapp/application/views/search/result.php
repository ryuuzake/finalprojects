<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Search Results</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('search')?>">Search</a></li>
                <li class="breadcrumb-item active">Result</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Search Result for "<?= $search ?>"</h3>

                  <div class="card-tools">
                    <?= $pagination ?>
                    <ul class="pagination pagination-sm float-right">
                      <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                    </ul>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                  <table class="table">
                    <thead>
                      <tr>
                        <th style="width: 10px">No</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Abstract</th>
                        <th>Author</th>
                        <th>Organization</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $i = 1;
                        foreach ($items as $item): 
                      ?>
                      <tr>
                        <td><?= $i ?></td>
                        <td><a href="<?= base_url('file/view/'.$item->uploadid) ?>"><?= $item->title ?></a></td>
                        <td><a href="<?= base_url('search/by_category/'.$item->slug) ?>"><?= $item->cat_name ?></a></td>
                        <td><?= $item->abstract ?></td>
                        <td><?= $item->author ?></td>
                        <td><?= $item->organization ?></td>
                      </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->