<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <button class="navbar-toggler" data-toggle="collapse" data-target="#collapse_target">
        <span class="navbar-toggler-icon"></span>
      </button>
      
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <li class="nav-item dropdown">
          <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Browse</a>
          <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
            <li><a href="<?= base_url('search/by_year') ?>" class="dropdown-item">By Year</a></li>
            <li><a href="<?= base_url('search/by_subject') ?>" class="dropdown-item">By Subject</a></li>
          </ul>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="<?= base_url('repo/about') ?>" class="nav-link">About</a>
        </li>
      </ul>
    </nav>  

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a class="brand-link elevation-4" href="<?= base_url() ?>">
        <img src="<?= base_url('assets/img/logo.png') ?>"
          alt="Logo"
          class="brand-image img-circle elevation-3"
          style="opacity: .8">
        <span class="brand-text text-brown">Quan<span class="text-white">⁍</span>Repo<span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <?php if($this->session->userdata('logged_in')): ?>
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="<?= $this->session->userdata('photo') ?>" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="<?= base_url('user') ?>" class="d-block"><?= $this->session->userdata('full_name') ?></a>
          </div>
        </div>

        <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <li class="nav-item">
                <a href="<?= base_url('auth/logout') ?>" class="nav-link">
                  <i class="nav-icon fa fa-sign-out"></i>
                  <p>Log out</p>
                </a>
              </li>
            </ul>
          </nav>
        <!-- /.sidebar-menu -->
        <?php else: ?>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item">
              <a href="<?= base_url('auth/login')?>" class="nav-link">
                <i class="nav-icon fa fa-sign-in"></i>
                <p>Login</p>
              </a>
            </li>
          </ul>
        </nav>
        <?php endif; ?>
      </div>
      <!-- /.sidebar -->
    </aside>