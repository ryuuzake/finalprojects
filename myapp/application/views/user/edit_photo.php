<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <div class="container bg-light">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Edit Photo</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url('user') ?>">Profile</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('user/edit_profile') ?>">Edit Profile</a></li>
                <li class="breadcrumb-item active">Photo</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
          <?php echo validation_errors(); ?>
          <div class="<?= $this->session->flashdata('feedback_class') ?>">
            <p><?= $this->session->flashdata('feedback') ?></p>
          </div>
          </div>
          <div class="row">
            <div class="col-md">
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Edit Photo</h3>
                </div>
                <form action="<?= base_url('user/do_upload_photo')?>" method="post" role="form" enctype="multipart/form-data">
                  <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>" />
                  <div class="card-body">
                    <div class="form-group">
                      <label for="uploadPhoto">Upload photo</label>
                      <?php if($photo): ?>
                      <p>Already upload photo</p>
                      <?php else: ?>
                      <div class="input-group">
                        <div class="custom-file">    
                          <input type="file" class="custom-file-input" id="uploadPhoto" name="photo">
                          <label class="custom-file-label" for="uploadPhoto">Choose photo</label>
                        </div>
                      </div>
                      <?php endif; ?>
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>