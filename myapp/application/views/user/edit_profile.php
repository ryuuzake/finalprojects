<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <div class="container bg-light">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Edit Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?= base_url() ?>user">Profile</a></li>
                <li class="breadcrumb-item active">Edit Profile</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    
      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
          <?php echo validation_errors(); ?>
          <div class="<?= $this->session->flashdata('feedback_class') ?>">
            <p><?= $this->session->flashdata('feedback') ?></p>
          </div>
          </div>
          <div class="row">
            <div class="col-md">
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Edit Profile</h3>
                </div>
                <form action="" method="post" role="form">
                  <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>" />
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" name="email" class="form-control" id="exampleInputEmail1" value="<?= $email ?>" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                      <label for="username">Username</label>
                      <input type="text" class="form-control" name="username" id="username" value="<?= $username ?>" placeholder="Enter Username"> 
                    </div>
                    <div class="form-group">
                      <label for="firstname">First Name</label>
                      <input type="text" class="form-control" name="firstname" id="firstname" value="<?= $fname ?>" placeholder="Enter First Name">
                    </div>
                    <div class="form-group">
                      <label for="lastname">Last Name</label>
                      <input type="text" class="form-control" name="lastname" id="lastname" value="<?= $lname ?>" placeholder="Enter Last Name">
                    </div>
                    <div class="form-group">
                      <label for="gender">Gender</label>
                      <select class="form-control" name="gender" id="gender">
                        <option value="1" <?php if($gender) echo 'selected'; ?>>Male</option>
                        <option value="0" <?php if(!$gender) echo 'selected'; ?>>Female</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="phone">Phone Number</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-phone"></i></span>
                        </div>
                        <input type="text" class="form-control" name="phone" value="<?= $phone ?>" placeholder="Enter Phone Number">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Birthday</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </div>
                        <input type="text" id="datemask" name="birthday" value="<?= $birthday ?>" placeholder="Enter Birthday" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="birthplace">Birth Place</label>
                      <input type="text" class="form-control" name="birthplace" id="birthplace" value="<?= $birthplace ?>" placeholder="Enter Birth Place">
                    </div>
                    <div class="form-group">
                      <label for="address">Address</label>
                      <input type="text" name="address" id="address" class="form-control" value="<?= $address ?>" placeholder="Enter Address">
                    </div>
                    <div class="form-group">
                      <label for="bio">Biodata</label>
                      <textarea class="form-control" name="bio" id="bio" rows="3"><?= $bio ?></textarea>
                    </div>

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url() ?>assets/js/moment.min.js"></script>
    <script src="<?= base_url() ?>assets/js/jquery.inputmask.bundle.min.js"></script>
    <script src="<?= base_url() ?>assets/js/adminlte.min.js"></script>
    <script>
      $(function () {
        //Datemask dd-mm-yyyy
        $('#datemask').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' })

        $('[data-mask]').inputmask()
      })
    </script>
  </body>
</html>