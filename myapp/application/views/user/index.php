<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item active">Profile</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md">
              <!-- Widget: user widget style 1 -->
              <div class="card card-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-info">
                  <h3 class="widget-user-username"><?= $this->session->userdata('full_name') ?></h3>
                  <h5 class="widget-user-desc"><?= $this->session->userdata('role') ?></h5>
                </div>
                <div class="widget-user-image">
                  <img class="img-circle elevation-2" src="<?= $this->session->userdata('photo') ?>" alt="User Avatar">
                </div>
                <div class="card-footer">
                  <div class="row">
                    <div class="col-sm-4 border-right">
                      <div class="description-block">
                        <h5 class="description-header"><?= $file_uploaded ?></h5>
                        <span class="description-text">FILES UPLOADED</span>
                      </div>
                      <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4 border-right">
                      <div class="description-block">
                        <h5 class="description-header"><?= $verified ?></h5>
                        <span class="description-text">VERIFIED</span>
                      </div>
                      <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-4">
                      <div class="description-block">
                        <h5 class="description-header"><?= $pending ?></h5>
                        <span class="description-text">PENDING</span>
                      </div>
                      <!-- /.description-block -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
              </div>
              <!-- /.widget-user -->
            </div>
          </div>
          <?php if(!$profile_completed): ?>
          <div class="row">
            <div class="col">
              <div class="card">
                <div class="card-header">
                  <h5 class="card-title text-bold">Complete your profile</h5>
                </div>
                <div class="card-body">
                  <p class="card-text">
                    Completed your profile so other user could know more about you
                  </p>
                  <a href="<?= base_url('user/edit_profile') ?>" class="card-link">Edit your profile</a>
                </div>
              </div>
            </div>
          </div>
          <?php endif; ?>
          <div class="row">
            <div class="col">
              <div class="card card-primary card-outline card-tabs">
                <div class="card-header p-0 pt-1 border-bottom-0">
                  <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="true">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="custom-tabs-two-messages-tab" data-toggle="pill" href="#custom-tabs-two-messages" role="tab" aria-controls="custom-tabs-two-messages" aria-selected="false">Messages</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="custom-tabs-two-settings-tab" data-toggle="pill" href="#custom-tabs-two-settings" role="tab" aria-controls="custom-tabs-two-settings" aria-selected="false">Settings</a>
                    </li>
                  </ul>
                </div>
                <div class="card-body">
                  <div class="tab-content" id="custom-tabs-two-tabContent">
                    <div class="tab-pane fade show active" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                      <div class="row">
                        <div class="col-md-3">
                          <a href="<?= base_url('file/upload') ?>" class="btn btn-primary btn-block mb-3">Upload File</a>

                          <div class="card">
                            <div class="card-header">
                              <h3 class="card-title">Labels</h3>

                              <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                              </div>
                            </div>
                            <div class="card-body p-0">
                              <ul class="nav nav-pills flex-column">
                                <li class="nav-item">
                                  <a href="<?= base_url('user?filter=3') ?>" class="nav-link">
                                    <i class="fa fa-circle text-primary"></i>
                                    All Files
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a href="<?= base_url('user?filter=2') ?>" class="nav-link">
                                    <i class="fa fa-circle text-danger"></i>
                                    Decline
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a href="<?= base_url('user?filter=1') ?>" class="nav-link">
                                    <i class="fa fa-circle text-warning"></i>
                                    Pending
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a href="<?= base_url('user?filter=0') ?>" class="nav-link">
                                    <i class="fa fa-circle text-secondary"></i>
                                    Draft
                                  </a>
                                </li>
                              </ul>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-9">
                          <div class="card card-primary card-outline">
                            <div class="card-header">
                              <h3 class="card-title">Documents</h3>

                              <div class="card-tools">
                                <div class="input-group input-group-sm">
                                  <input type="text" class="form-control" placeholder="Search Documents">
                                  <div class="input-group-append">
                                    <div class="btn btn-primary">
                                      <i class="fa fa-search"></i>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!-- /.card-tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-0">
                              <div class="mailbox-controls">
                                <!-- Check all button -->
                                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square"></i>
                                </button>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-alt"></i></button>
                                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                                </div>
                                <!-- /.btn-group -->
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-sync-alt"></i></button>
                                <div class="float-right">
                                  1-50/200
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                                  </div>
                                  <!-- /.btn-group -->
                                </div>
                                <!-- /.float-right -->
                              </div>
                              <div class="table-responsive mailbox-messages">
                                <table class="table table-hover table-striped">
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>Status</th>
                                      <th>Title</th>
                                      <th>Abstract</th>
                                      <th>Date</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php 
                                    $i = 1;
                                    foreach ($items as $item): 
                                  ?>
                                  <tr>
                                    <td class="mailbox-star"><?= $i ?></td>
                                    <?php if ($item->status == '0') : ?>
                                    <td class="mailbox-name text-secondary">Draft</td>
                                    <?php elseif($item->status == '1'): ?>
                                    <td class="mailbox-name text-warning">Pending</td>
                                    <?php elseif($item->status == '2'): ?>
                                    <td class="mailbox-name text-danger">Decline</td>
                                    <?php elseif($item->status == '3'): ?>
                                    <td class="mailbox-name text-success">Verified</td>
                                    <?php endif; ?>
                                    <td class="mailbox-name"><a href="<?= base_url('file/view/'.$item->uploadid) ?>"><?= $item->title ?></a></td>
                                    <td class="mailbox-subject"><?= $item->abstract ?></td>
                                    <td class="mailbox-date"><?= $item->updated_at ?></td>
                                  </tr>
                                  <?php 
                                    $i++;
                                    endforeach; 
                                  ?>
                                  </tbody>
                                </table>
                                <!-- /.table -->
                              </div>
                              <!-- /.mail-box-messages -->
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                        <!-- /.col -->
                      </div>
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
                        Mauris tincidunt mi at erat gravida, eget tristique urna bibendum. Mauris pharetra purus ut ligula tempor, et vulputate metus facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas sollicitudin, nisi a luctus interdum, nisl ligula placerat mi, quis posuere purus ligula eu lectus. Donec nunc tellus, elementum sit amet ultricies at, posuere nec nunc. Nunc euismod pellentesque diam. 
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-two-messages" role="tabpanel" aria-labelledby="custom-tabs-two-messages-tab">
                        Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt eleifend ac ornare magna. 
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-two-settings" role="tabpanel" aria-labelledby="custom-tabs-two-settings-tab">
                        Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis. 
                    </div>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            </div>
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>