<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="background-color: rgba(0,0,0,0.4)">

      <!-- Main content -->
      <section class="content">

        <div class="row p-5">
          <div class="col-4 mr-auto ml-auto">
            <!-- Default box -->
            <div class="card" style="background-color: #53535387">
              <div class="card-body">
              <h1 class="text-center" style="color:#FFF; font-family: Rockwell;">Quantum Repository</h1>
                <!-- SEARCH FORM -->
                <form class="form-inline mt-5" action="<?= base_url('search') ?>">
                  <div class="input-group navbar-light input-group-lg mr-auto ml-auto">
                    <input class="form-control form-control-navbar" type="search" name="q" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                      <button class="btn btn-navbar" type="submit">
                        <i class="fa fa-search"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->