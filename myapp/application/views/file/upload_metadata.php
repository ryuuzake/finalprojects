<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">Add Metadata</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?= base_url('user') ?>">Profile</a></li>
                  <li class="breadcrumb-item"><a href="<?= base_url('file/upload') ?>">Upload File</a></li>
                  <li class="breadcrumb-item active">Add Metadata</li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md">
                <?php if($this->session->flashdata('feedback_class')): ?>
                <div class="alert <?= $this->session->flashdata('feedback_class') ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa <?= $this->session->flashdata('feedback_icon') ?>"></i> Alert!</h5>
                  <?= $this->session->flashdata('feedback') ?>
                </div>
                <?php endif; ?>
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Add a metadata to your entry</h3>
                  </div>
                  <form id="uploadFileForm" action="" method="post" role="form">
                    <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>" />
                    <input type="hidden" name="docid" value="<?= $docid ?>"  />
                    <div class="card-body">
                      <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" placeholder="Enter Document Title">
                      </div>
                      <div class="form-group">
                        <label>Category</label>
                        <select name="category" class="form-control select2bs4" style="width: 100%;">
                          <?php foreach ($categories as $category): ?>
                          <option value="<?= $category->catid ?>"><?= $category->cat_name ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Author</label>
                        <textarea name="author" class="form-control" rows="3" placeholder="Enter author name"></textarea>
                      </div>
                      <div class="form-group">
                        <label>Organization</label>
                        <textarea name="organization" class="form-control" rows="3" placeholder="Enter Organization"></textarea>
                      </div>
                      <div class="form-group">
                        <label>Abstract</label>
                        <textarea name="abstract" class="form-control" rows="3" placeholder="Enter Abstract"></textarea>
                      </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
      </div>