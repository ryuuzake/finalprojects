<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">Upload File</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?= base_url('user') ?>">Profile</a></li>
                  <li class="breadcrumb-item active">Upload File</li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md">
                <?php if($this->session->flashdata('feedback_class')): ?>
                <div class="alert <?= $this->session->flashdata('feedback_class') ?> alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h5><i class="icon fa <?= $this->session->flashdata('feedback_icon') ?>"></i> Alert!</h5>
                  <?= $this->session->flashdata('feedback') ?>
                </div>
                <?php endif; ?>
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Add a new document to your entry</h3>
                  </div>
                  <form id="uploadFileForm" action="" method="post" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="<?= $csrf['name'] ?>" value="<?= $csrf['hash'] ?>" />
                    <div class="card-body">  
                      <div class="form-group">
                        <label for="uploadPhoto">Upload Document</label>
                        <div class="input-group">
                          <div class="custom-file">    
                            <input type="file" class="custom-file-input" id="uploadFile" name="file">
                            <label class="custom-file-label" for="uploadPhoto">Choose Document</label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Language</label>
                        <select name="language" class="form-control">
                          <option value="en">English</option>
                          <option value="id">Bahasa Indonesia</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Document Release Date</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                          </div>
                          <input type="text" id="datemask" name="date_embargo" placeholder="Enter Date" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                        </div>
                      </div>
                      <div id="progressFile" class="progress" style="display: none">
                        <div class="progress-bar bg-primary progress-bar-striped" role="progressbar"
                            aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                          <span class="sr-only">0% Complete (success)</span>
                        </div>
                      </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
      </div>
      <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js" integrity="sha384-FzT3vTVGXqf7wRfy8k4BiyzvbNfeYjK+frTVqZeNDFl8woCbF0CYG6g2fMEFFo/i" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
      <script src="<?= base_url('assets/js/adminlte.min.js') ?>"></script>
      <script src="<?= base_url('assets/js/uploadfile.js') ?>"></script>
      <script src="<?= base_url('assets/js/jquery.inputmask.bundle.min.js') ?>"></script>
      <script>
        $(function () {
          //Datemask dd-mm-yyyy
          $('#datemask').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })

          $('[data-mask]').inputmask()
        })
      </script>
    </div>
  </body>
</html>
    