<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $upload->title ?></h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?= base_url('file') ?>">Documents</a></li>
                  <li class="breadcrumb-item active"><?= $upload->title ?></li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">

        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Detail</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                <div class="row">
                  <div class="col-12">
                    <h4>File Preview</h4>
                    <div class="card">
                      <div class="card-body">
                        <?php if(strpos($document->mime_type, 'image') === 0): ?>
                        <img src="<?= base_url("uploads/documents/$document->content") ?>" class="img-fluid" alt="<?= $document->content ?>">
                        <?php elseif(strpos($document->mime_type, 'video') === 0): ?>
                        <div class="embed-responsive embed-responsive-16by9">
                          <iframe class="embed-responsive-item" src="<?= base_url("uploads/documents/$document->content") ?>"></iframe>
                        </div>
                        <?php else: ?>
                          <button type="button" class="btn btn-block btn-default"><i class="fa fa-file" aria-hidden="true"></i> <?= $document->content ?></button>
                        <?php endif; ?>
                      </div>
                    </div>
                  </div>  
                  <div class="col-12">
                      <h4>Recent Activity</h4>
                        <div class="post">
                          <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
                            <span class="username">
                              <a href="#">Jonathan Burke Jr.</a>
                            </span>
                            <span class="description">Shared publicly - 7:45 PM today</span>
                          </div>
                          <!-- /.user-block -->
                          <p>
                            Lorem ipsum represents a long-held tradition for designers,
                            typographers and the like. Some people hate it and argue for
                            its demise, but others ignore.
                          </p>

                          <p>
                            <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 1 v2</a>
                          </p>
                        </div>

                        <div class="post clearfix">
                          <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="../../dist/img/user7-128x128.jpg" alt="User Image">
                            <span class="username">
                              <a href="#">Sarah Ross</a>
                            </span>
                            <span class="description">Sent you a message - 3 days ago</span>
                          </div>
                          <!-- /.user-block -->
                          <p>
                            Lorem ipsum represents a long-held tradition for designers,
                            typographers and the like. Some people hate it and argue for
                            its demise, but others ignore.
                          </p>
                          <p>
                            <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 2</a>
                          </p>
                        </div>

                        <div class="post">
                          <div class="user-block">
                            <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
                            <span class="username">
                              <a href="#">Jonathan Burke Jr.</a>
                            </span>
                            <span class="description">Shared publicly - 5 days ago</span>
                          </div>
                          <!-- /.user-block -->
                          <p>
                            Lorem ipsum represents a long-held tradition for designers,
                            typographers and the like. Some people hate it and argue for
                            its demise, but others ignore.
                          </p>

                          <p>
                            <a href="#" class="link-black text-sm"><i class="fas fa-link mr-1"></i> Demo File 1 v1</a>
                          </p>
                        </div>
                    </div>
                  </div>
              </div>
              <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                <h3 class="text-black"><?= $upload->title ?></h3>
                <p class="text-muted"><?= $upload->abstract ?></p>
                <br>
                <div class="text-muted">
                  <p class="text-sm">Author
                    <b class="d-block"><?= $upload->author ?></b>
                  </p>
                  <p class="text-sm">Organization
                    <b class="d-block"><?= $upload->organization ?></b>
                  </p>
                  <p class="text-sm">Category
                    <b class="d-block"><?= $category->cat_name ?></b>
                  </p>
                </div>

                <h5 class="mt-5 text-muted">File</h5>
                <ul class="list-unstyled">
                  <li>
                    <a href="<?= base_url("uploads/documents/$document->content") ?>" class="btn-link text-secondary"><i class="fa fa-file"></i> <?= $document->content ?></a>
                  </li>
                </ul>
                <div class="text-center mt-5 mb-3">
                  <a href="#" class="btn btn-sm btn-primary">Add files</a>
                  <a href="#" class="btn btn-sm btn-warning">Edit</a>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        </section>
        <!-- /.content -->
      </div>