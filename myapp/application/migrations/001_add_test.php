<?php
    class Migration_Add_test extends CI_Migration {
        public function up(){
            $this->dbforge->create_database('repo');
            
            $this->dbforge->add_field(
                array(
                    'id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => true,
                        'auto_increment' => true
                    ),
                    'test' => array(
                        'type' => 'VARCHAR',
                        'constraint' => 2
                    )
                )
            );

            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->create_table('test');
        }

        public function down(){
            $this->dbforge->drop_table('test');
            $this->dbforge->drop_database('repo');
        }
    }
?>