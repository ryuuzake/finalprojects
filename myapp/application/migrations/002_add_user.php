<?php
    class Migration_Add_user extends CI_Migration {
        public function up(){
            $this->dbforge->add_field(
                array(
                    'userid' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                        'unsigned' => true,
                        'auto_increment' => true
                    ),
                    'username' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '100'
                    ),
                    'password' => array(
                        'type' => 'varchar'
                    ),
                    'email' => array(
                        'type' => 'varchar'
                    ),
                    'joined' => array(
                        'type' => 'datetime'
                    ),
                    'first_name' => array(
                        'type' => 'varchar'
                    ),
                    'last_name' => array(
                        'type' => 'varchar'
                    ),
                    'phone_no' => array(
                        'type' => 'varchar'
                    ),
                    'birthday' => array(
                        'type' => 'datetime'
                    ),
                    'birth_place' => array(
                        'type' => 'varchar'
                    ),
                    'address' => array(
                        'type' => 'varchar'
                    ),
                    'photo' => array(
                        'type' => 'longblob'
                    ),
                    'bio' => array(
                        'type' => 'text',
                    ),
                    'gender' => array(
                        'type' => 'boolean'
                    )
                )
            );

            $this->dbforge->add_key('userid', TRUE);
            $this->dbforge->create_table('user');
        }

        public function down(){
            $this->dbforge->drop_table('user');
        }
    }
?>