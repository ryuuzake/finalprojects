<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_model extends CI_Model {
    private $_table = "upload";

    public $user_id;
    public $doc_id;
    public $cat_id;
    public $title;
    public $status;
    public $author;
    public $organization;
    public $abstract;

    public function get_all(){
        return $this->db->get($this->_table)->result();
    }

    public function get_all_by_userid($id){
        return $this->db->get_where($this->_table, ["user_id" => $id])->result();
    }

    public function get_filter_status_by_userid($filter,$id){
        return $this->db->get_where($this->_table, ["user_id" => $id, "status" => $filter])->result();
    }

    public function get_document_count_by_userid($id){
        $result = array(
            'all' => $this->db->get_where($this->_table, ['user_id' => $id])->num_rows(),
            'verified' => $this->db->get_where($this->_table, ['user_id' => $id, 'status' => '3'])->num_rows(),
            'pending' => $this->db->get_where($this->_table, ['user_id' => $id, 'status' => '2'])->num_rows(),
        );
        return $result;
    }

    public function get_by_id($id){
        return $this->db->get_where($this->_table, ["uploadid" => $id])->row();
    }

    public function save(){
        $post = $this->input->post();
        $this->user_id = $this->session->userdata('userid');
        $this->doc_id = $post['docid'];
        $this->cat_id = $post['category'];
        $this->title = $post['title'];
        $this->author = $post['author'];
        $this->organization = $post['organization'];
        $this->abstract = $post['abstract'];
        $this->db->insert($this->_table, $this);
        return $this->db->insert_id();
    }

    public function update(){
        $post = $this->input->post();
        // $prev_model = $this->get_by_id($post['id']);
        $this->user_id = $this->session->userdata('userid');
        $this->doc_id = $post['docid'];
        $this->cat_id = $post['category'];
        $this->title = $post['title'];
        $this->author = $post['author'];
        $this->organization = $post['organization'];
        $this->abstract = $post['abstract'];
        $this->db->update($this->_table, $this, ["uploadid" => $post['id']]);
    }

    public function delete($id){
        return $this->db->delete($this->_table, ["uploadid" => $id]);
    }

    public function search_simple($limit = NULL, $start = NULL){
        $this->db->select('*')
                 ->join('category', 'category.catid = upload.cat_id')
                 ->like('title', $this->input->get('q'));
        
        if($limit != NULL){
            $this->db->limit($limit, $start);
            return $this->db->get('upload')->result();
        } else {
            return $this->db->count_all_results('upload');
        }
    }
}