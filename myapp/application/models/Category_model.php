<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {
    private $_table = "category";

    public $catid;
    public $cat_name;
    public $slug;

    public function get_all(){
        return $this->db->get($this->_table)->result();
    }

    public function get_by_id($id){
        return $this->db->get_where($this->_table, ['catid' => $id])->row();
    }

    public function save(){
        $this->load->helper('url');

        $slug = url_title($this->input->post('category'), 'dash', TRUE);
        
        $this->catid = '';
        $this->cat_name = $this->input->post('category');
        $this->slug = $slug;
    }
}