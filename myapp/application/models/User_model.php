<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
    public function get_user($email){
        $query = $this->db->get_where('user', array('email' => $email));
        return $query->row_array();
    }

    public function get_role($userid){
        $this->db->select('role');
        $query = $this->db->get_where('user_roles', array('userid' => $userid));
        return $query->row_array();
    }

    public function get_users(){
        return $this->db->get('user')->result_array();
    }

    public function create_user($email, $pass){
        $this->load->helper('date');

        $data = array(
            'email' => $email,
            'password' => $pass
        );

        return $this->db->insert('user', $data);
    }

    public function get_completed_profile($user){
        foreach ($user as $key => $value){
            if($value == NULL) return FALSE;
        }
        return TRUE;
    }

    public function update_profile($email){
        try {
            $this->db->where('email', $email);
            $data = array(
                'email' => $this->input->post('email'),
                'username' => $this->input->post('username'),
                'first_name' => $this->input->post('fname'),
                'last_name' => $this->input->post('lname'),
                'gender' => $this->input->post('gender'),
                'phone_no' => $this->input->post('phone'),
                'birthday' => $this->input->post('birthday'),
                'birth_place' => $this->input->post('birthplace'),
                'address' => $this->input->post('address'),
                'bio' => $this->input->post('bio')
            );
            $this->db->update('user', $data);
            return TRUE;
        } catch (\Throwable $th) {
            return FALSE;
        }
    }

    public function update_photo($email, $loc){
        $this->db->where('email', $email);
        $this->db->update('user', array('photo' => $loc));
    }
}