<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_model extends CI_Model {
    private $_table = "document";

    public $rev_number;
    public $mime_type;
    public $languange;
    public $format;
    public $date_embargo;
    public $content;

    public function get_all(){
        return $this->db->get($this->_table)->result();
    }

    public function get_by_id($id){
        return $this->db->get_where($this->_table, ['docid' => $id])->row();
    }

    public function save(){
        $this->load->helper('date');
        $post = $this->input->post();
        $file = $this->_uploadFile();

        $this->rev_number = 1;
        $this->mime_type = $file['file_type'];
        $this->languange = $post['language'];
        $this->format = $file['file_ext'];
        $this->date_embargo = mdate('%Y-%m-%d', strtotime($post['date_embargo']));
        $this->content = $file['file_name'];

        $this->db->insert($this->_table, $this);
        return $this->db->insert_id();
    }

    public function delete($id){
        $this->_deleteFile();
        return $this->db->delete($this->_table, ["docid" => $id]);
    }

    private function _uploadFile(){
        $config['upload_path'] = './uploads/documents/';
        $config['allowed_types'] = 'gif|jpg|png|mp4|webm|ogg|pdf|txt|text|m4a|mp3|ppt|pptx|xls|xlsx|doc|docx';
        $config['max_size'] = 5120;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('file')) {
            $this->session->set_flashdata('feedback', 'File '.$this->upload->data('file_name').' successfully uploaded');
            $this->session->set_flashdata('feedback_class', 'alert-success');
            $this->session->set_flashdata('feedback_icon', 'fa-check');
            
            return $this->upload->data();
        } else {
            $this->session->set_flashdata('feedback', $this->upload->display_errors());
            $this->session->set_flashdata('feedback_class', 'alert-danger');
            $this->session->set_flashdata('feedback_icon', 'fa-ban');
            redirect(base_url('file/upload'));
        }
    }

    private function _deleteFile($id){
        $document = $this->get_by_id($id);
        if($document->content){
            return unlink('./uploads/documents/'.$document->content);
        }
    }
}