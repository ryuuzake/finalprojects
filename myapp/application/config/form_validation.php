<?php
$config = array(
    'login' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email',
            'errors' => array(
                'required' => '%s is required',
                'valid_email' => '%s is not valid'
            )
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required',
            'errors' => array(
                'required' => '%s is required'
            )
        )
    ),
    'edit_profile' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'trim'
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email',
            'errors' => array(
                'required' => '%s is required',
                'valid_email' => '%s is not valid'
            )
        ),
        array(
            'field' => 'phone',
            'label' => 'Phone',
            'rules' => 'required|regex_match[/^[0-9]{12}$/]',
        )
    ),
    'register' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|callback_email_check',
            'errors' => array(
                'required' => '%s is required',
                'valid_email' => '%s is not valid',
                'email_check' => '%s already used'
            )
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required|min_length[8]|max_length[16]',
            'errors' => array(
                'required' => '%s is required',
                'min_length' => '%s must be 8 - 16 character long',
                'max_length' => '%s must be 8 - 16 character long'
            )
        ),
        array(
            'field' => 'repassword',
            'label' => 'Re-enter Password',
            'rules' => 'trim|required|matches[password]',
            'errors' => array(
                'required' => '%s is required',
                'matches' => 'Password don\'t match'
            )
        )
    ),
    'upload_metadata' => array(
        array(
            'field' => 'title',
            'label' => 'Title',
            'rules' => 'trim|required',
            'errors' => array(
                'required' => '%s is required',
            )
        ),
        array(
            'field' => 'category',
            'label' => 'Category',
            'rules' => 'trim|required',
            'errors' => array(
                'required' => '%s is required',
            )
        ),
    ),
    'upload_file' => array(
        array(
            'field' => 'language',
            'label' => 'Language',
            'rules' => 'trim'
        )
    ),
);